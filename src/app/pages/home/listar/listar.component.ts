import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { MainService } from '../../../services/main.service';

@Component({
  selector: 'listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.scss'],
  providers: [MainService]
})

export class ListarComponent implements OnInit {
  DataArrayLivros: any = [];
  DataLivros: any = {};

  constructor(public data: MainService) {
    this.listar();
  }

  ngOnInit() {
  }

  listar() {
    this.data.listarLivros().subscribe(
      data => {
        this.DataArrayLivros = data;
        console.log(data);
      }
    );
  }

  deletar(id: any) {
    this.data.deletarLivro(id).subscribe(
      data => {
        this.DataLivros = data;
        console.log(data);
        alert('Item deletado com sucesso');
        window.location.reload();
      }
    );
  }
}
