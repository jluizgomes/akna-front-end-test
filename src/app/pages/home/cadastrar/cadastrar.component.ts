import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Http } from '@angular/http';
import * as $ from 'jquery';

@Component({
  selector: 'cadastrar',
  templateUrl: './cadastrar.component.html',
  styleUrls: ['./cadastrar.component.scss']
})
export class CadastrarComponent implements OnInit {
  headers = new Headers({
    'accept': 'application/json'
  });

  constructor(private fbuilder: FormBuilder,
    private http: Http) { }

  ngOnInit() {

  }

  public cadForm = new FormGroup({
    titulo: new FormControl('', Validators.required),
    autor: new FormControl('', Validators.required),
    preco: new FormControl('', Validators.required)
  });

  salvar() {
    let formObj = this.cadForm.getRawValue();

    let serializedForm = JSON.stringify(formObj);

    console.log(formObj);

    this.http.post("http://localhost:8080/livros.php", serializedForm)
      .subscribe(
      data =>  window.location.reload(),
      error => console.error("couldn't post because", error)
      );
  }

  _keyPressText(event: any) {
    const pattern = /^[a-zA-Z\s]*$/;
    let inputChar = String.fromCharCode(event.charCode);

    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }
  }

  // _keyPressNumber(event: any) {
  //   const pattern = /[0-9]+([\.,][0-9]+)?/;
  //   let inputChar = String.fromCharCode(event.charCode);
  //   if (!pattern.test(inputChar)) {
  //     // invalid character, prevent input
  //     event.preventDefault();
  //   }
  // }
}
