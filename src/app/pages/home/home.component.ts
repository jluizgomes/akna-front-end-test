import { Component, OnInit } from '@angular/core';
import { MainService } from '../../services/main.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [MainService]
})

export class HomeComponent implements OnInit {

  DataArray: any = [];

  constructor(public data: MainService) {
    this.loadMenu();
  }

  ngOnInit() {
  }

  loadMenu() {
    this.data.menuData().subscribe(
      data => {
        this.DataArray = data;
        console.log(data);
      }
    );
  }
}
