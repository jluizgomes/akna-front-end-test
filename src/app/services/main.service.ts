import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class MainService {

  constructor(private http: Http) {
  }

  public menuData() {
    const url = "./assets/data/menu.json";
    const response = this.http.get(url).map(res => res.json());

    return response;
  }

  public listarLivros() {
    const url = "http://localhost:8080/livros.php";
    const response = this.http.get(url).map(res => res.json());

    return response;
  }

  public deletarLivro(id: any) {
    const url = "http://localhost:8080/livros.php";
    const response = this.http.delete(url + "/?id=" + id).map(res => res.json());

    return response;
  }

}
